package example;

public class Main {

    public static void main(String[] args) {
        int number = 642;
        int guesses[] = { 123, 456, 789, 152, 346, 273, 846, 742, 642 };

        NumberGuess numberGuess = new NumberGuess(number);

        for (int guess : guesses) {
            GuessResult result = numberGuess.guess(guess);
            System.out.println(guess + " is " + result.getStrike() + " strike, " + result.getBall() + " ball");
//            System.out.format("%d is %d strike, %d ball\n", guess, result.getStrike(), result.getBall());
        }
    }
}

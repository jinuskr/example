package example;

public class NumberGuess {
    int number;

    public NumberGuess(int number) {
        this.number = number;
    }

    public GuessResult guess(int guess) {
        int strike = 0;
        int ball = 0;

        int digits[][] = {makeDigits(number), makeDigits(guess)};

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (digits[0][i] == digits[1][j]) {
                    if (i == j) strike++;
                    else ball++;
                }
            }
        }

        return new GuessResult(strike, ball);
    }

    private int[] makeDigits(int number) {
        int digits[] = new int[3];

        for (int i = 0; i < 3; i++) {
            digits[i] = number % 10;
            number = number / 10;
        }

        return digits;
    }
}
